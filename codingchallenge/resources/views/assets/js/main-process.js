$(document).ready(function() {

    $( "#estimateBtn" ).click(function() {
        if($('#mass').val() != "" && $('#postcode').val() != ""){
         $('#resultsTable').empty();
            switch ($(this).attr('dataval')){
                    
                case "rtt" :
                     estimateRate_rtt();
                    break;
                    
                case "courierit" :
                    estimateRate_courierit();
                default :
                    break;
                    
            }
           
           
            return false;
        }

        
    });
    
        $( "#compareBtn" ).click(function() {
        if($('#mass').val() != "" && $('#postcode').val() != ""){

            compare_rates();
          // window.location.href = "./compare-results.html";
            return false;
        }

        
    });
    
    
    
});


async function compare_rates(){
     $("#inputDetails").hide()
     $(".results").show()

    console.log('COMPARE')
    console.log('test')
    var postcode = $('#postcode').val();
    var mass = $('#mass').val();
    var address = $('#address1').val() + " " + $('#address2').val();
    var city = $('#city').val();
    var country = $('#country').val();
    
    
    console.log("test")
     //Replace point with comma
    mass = mass.replace(".", ",");
    console.log(mass);
    
            
      
    var isAfterHours = 0;
    var special = 'N';

            
    if(  $('#afterHours').is(":checked")){
        special = "AH";
        isAfterHours = 1;
    }
    else if($('#saturday').is(":checked")){
        special = "S"
    }
            
    
    var serviceLevel = $("#serviceLevelSelect").val();
    console.log(serviceLevel)
    var SL;
    
    switch (serviceLevel){
        case '1':
           SL = "SDA"
            break;
            
        case '2':
            SL = "ONC"
            break;
            
        case '3':
            SL = "ECO"
            break;
    }
    
    var apiurl = "http://localhost:8000/api/compare/"
    apiurl = apiurl + postcode + "/" + mass + "/" + SL + '/' +  isAfterHours + '/' + special;
    console.log(apiurl)
    
     var request = new XMLHttpRequest();
    
    let response = await fetch(apiurl);
    let data = await response.json();
    console.log(data);
    
    rttData = data['rtt'];
    dsvData = data['dsv'];
    
    var winner = data['winner'];
    if(winner == 'rtt'){
        $("#winner").html("RTT Logistics");
    }
    else if(winner == 'dsv'){
        $("#winner").html("DSV Logistics");
    }
    
    
    var RTTtotalcost = Math.round(rttData['Total_Cost'] * 100) / 100
    $("#RTTtotalVal").html("R")
    $("#RTTtotalVal").append(RTTtotalcost)
    var rttHTML = '';
          
    Object.keys(rttData).forEach(function(key) {
    console.log(key, rttData[key]);
    rttHTML = rttHTML + '<tr><td>' + key + '</td><td>' + rttData[key] + '</td></tr>';
            });

                
    $('#RTTresultsTable').append(rttHTML);    
    
    var DSVtotalcost = Math.round(dsvData['Total_Cost'] * 100) / 100
    $("#DSVtotalVal").html("R")
    $("#DSVtotalVal").append(DSVtotalcost)
    var dsvHTML = '';
          
    Object.keys(dsvData).forEach(function(key) {
    console.log(key, dsvData[key]);
    dsvHTML = dsvHTML + '<tr><td>' + key + '</td><td>' + dsvData[key] + '</td></tr>';
            });

                
    $('#DSVresultsTable').append(dsvHTML);    
    
}


async function estimateRate_courierit(){
    
    var result;
    var is_finished = false;
    
    var postcode = $('#postcode').val();
    var mass = $('#mass').val();
    var address = $('#address1').val() + " " + $('#address2').val();
    var city = $('#city').val();
    var country = $('#country').val();
    
    var request = new XMLHttpRequest();
    
    //Replace point with comma
    mass = mass.replace(".", ",");
    console.log(mass);
    
            
      
            var isAfterHours = 0;
            var special = 'N';
            
            if(  $('#afterHours').is(":checked")){
                special = "AH";
            }
    else if($('#saturday').is(":checked")){
        special = "S"
    }
            
              
    
            var serviceLevel = $("#serviceLevelSelect").val();
      

            

        //   var apiurl = "http://localhost:8000/api/courierit/"

            var apiurl = "http://localhost:8000/api/rtt/"

            
            apiurl = apiurl + postcode + "/" + mass + "/" + serviceLevel + '/' + special + '/1/true';
            console.log(apiurl)
            

            let response = await fetch(apiurl);
            let data = await response.json();
    
            console.log(data['Total Cost'])
            var totalcost = Math.round(response['Total Cost'] * 100) / 100
            $("#totalVal").html("R")
            $("#totalVal").append(totalcost)
            var trHTML = '';
            Object.keys(data).forEach(function(key) {
            console.log(key, data[key]);
            trHTML = trHTML + '<tr><td>' + key + '</td><td>' + data[key] + '</td></tr>';
                    });
                

                

                $('#resultsTable').append(trHTML);
                downloadPdf(data, 'rtt', serviceLevel)
             
                result = data;
               
           
        
}




async function estimateRate_rtt(){
    
    var result;
    var is_finished = false;
    var postcode = $('#postcode').val();
    var mass = $('#mass').val();
    var address = $('#address1').val() + " " + $('#address2').val();
    var city = $('#city').val();
    var country = $('#country').val();
    
    var request = new XMLHttpRequest();
    
    //Replace point with comma
    mass = mass.replace(".", ",");
    console.log(mass);
    
            
        //RTT
            var isAfterHours = 0;
            var special = 'N';

              
   
            var serviceLevel = $("#serviceLevelSelect").val();
                        
            if(  $('#afterHours').is(":checked")){
                isAfterHours = 1;
            }
            
            if($('#homeDelivery').is(":checked")){
                special = 'H'
            }

      
            
            var apiurl = "http://localhost:8000/api/rtt/"
            
            apiurl = apiurl + postcode + "/" + mass + "/" + serviceLevel + '/' + isAfterHours + '/' + special + '/1/true/0';
            console.log(apiurl)
            

            let response = await fetch(apiurl);
            let data = await response.json();
    
            console.log(data['Total_Cost'])
            var totalcost = Math.round(response['Total_Cost'] * 100) / 100
            $("#totalVal").html("R")
            $("#totalVal").append(totalcost)
            var trHTML = '';
          
            Object.keys(data).forEach(function(key) {
            console.log(key, data[key]);
            trHTML = trHTML + '<tr><td>' + key + '</td><td>' + data[key] + '</td></tr>';
                    });

                
                $('#resultsTable').append(trHTML);
                downloadPdf(data, 'rtt', serviceLevel)
           
        
     
    
    
}


function downloadPdf(data, company, serviceLevel){
    
        var postcode = $('#postcode').val();
    var mass = $('#mass').val();
    var address = $('#address1').val() + " " + $('#address2').val();
    var city = $('#city').val();
    var country = $('#country').val();
    
        var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date+'_'+time;
    
var imageData = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD//gAfQ29tcHJlc3NlZCBieSBqcGVnLXJlY29tcHJlc3P/2wCEAAQEBAQEBAQEBAQGBgUGBggHBwcHCAwJCQkJCQwTDA4MDA4MExEUEA8QFBEeFxUVFx4iHRsdIiolJSo0MjRERFwBBAQEBAQEBAQEBAYGBQYGCAcHBwcIDAkJCQkJDBMMDgwMDgwTERQQDxAUER4XFRUXHiIdGx0iKiUlKjQyNEREXP/CABEIAFwAcgMBIgACEQEDEQH/xAAcAAABBAMBAAAAAAAAAAAAAAAAAQUGBwIDCAT/2gAIAQEAAAAA6LAAUAECs3OEzWJytY5JozJ2O0xKDdIq/Nrq917Ye2X0p0YoEZyYnX3+qHOEu3ABCDDDbuZvJNcJIBE+WLQrLU3emx4rYNadkqFF0BeFY62302PFbCrPspROMJy51lrbfTY8VsKs+ylIxzLYWqstbb6bHithVn2UqVEx4aKz1NvqseK2FWXZShjgYCGeOeHoNagAAAL/AP/EABwBAAEFAQEBAAAAAAAAAAAAAAUBAgMEBgcACP/aAAgBAhAAAAD4yIthlWkMMaK7TnB1AibnSXqlfLZFe/SHA9DJc0OdG0JsPQyfNFc5U81v/8QAGwEAAgMBAQEAAAAAAAAAAAAAAgMABAUBBgf/2gAIAQMQAAAA+lphcjGKpKYNpj2ZVICO/dPyfK726GsjHqIc3Q1j5ySdD//EAEcQAAEDAwIBBQoKBwkBAAAAAAECAwQABQYHERITITF1tBQgIiMyNUFRYbUQNjdHc3R2gbGzCBUWJzM0cSRDRWRygsHCw9H/2gAIAQEAAT8A2HRtW1c1bCvur7q5q2rYVsK5q2FbCubf4PTWZZdkFnyLGsbx+DbnpN3YluBye6tptBj7HpTWD5e7lNmnXG4QUQn4MyREkhtzlWOJjnKm1+lNI1Dze6WyblmP4pCkY0wtxTYfkqTNksMHZbraRU/PbVDwdGdJQ47DdjIdZZHM4444eFLXN6eKl53mmPu2WbmmNwY1muUhqPy0OQp16I495AeBrJcvXj2S4nZ1ssdyXRUrul91ZSWURm+U3H/NYXlVxy5i5Xk25uLYy8pFrdUVcvJaR0vKSeYJPorEs1y/LpYuECzWpGPC4uxXeOUvu1lDJ4S4tHRufVVyymXCzjHMTbiNKYuUKXJW+oq5RCmAdgAKzXL87xZ8yWLHZ3rS/cmYUNxyU4HlF87ILiQNk01d8rt+N5DdcjtluZmwY8h+O1FfW604GWivZZVsRuRWKZHqDf27HdJVksbNonNNyFLamOKfSy4NwQgjp7zVWHBdzPBZd7sMu52ZmJcBKbjRlyOdXCEbhFaY2a4otGZQlW+XbbBNmPCzQ5gIeYjuoKTuk7lIJqyZNdsVwt7AJeKXd/IozUqFGSxGLkd8Ok8LvKdATsqrvgV4a0btmLxG+Xu1tRGlBlJ8t1l0uqbHr24qyO+y9TYtixWz47dYq1z4ki5vzIpZahts+EoBR8o1qpi5yzKNPLU83KFvcelplPMJO7bfCDsVdA32rTV+82cz8CvzD5ctG36vncmQ3Kgr8gBXRxo3pxcaXmlnuGA4/fbVeV3QIvDa4y2ITsUKIWt3pTz1fIUlzVvB5iIzq4zVquKVuhBKElQOwUqtV4sqXZLCiJHceWnI7W4UtoKiEpcJJIHoFZg049imUtNIW44u0zkoSkEqWpTKgAAK0tfxSzrsMZnEL9FyB+K3ElTHYz/c/KbeGd1qKQK9X/3f4dz8GY35zHMcuFxjpC5gSliG2ecLlPENtJ2/1GsSvn7Q49bbqtATIW3wSmxt4uSyS26jb2LFW++X+559fbMUGPZ7THj7+LbWX3ZKCpK+PjKkg9KABzgc9XzKTZ7parLGss24TZ7TrraIym0JShkgKK1OKSABvWTXN+zY5fbrGbWpyHCefSEBKiCgElXCsoBCPKI3FQpbr9liTVucbi4Lb6jw8AUothe/Dz7Vp/mv7XW5AKFvPxo7InykICGO6l85ZQncHiSOmsyzaDhTESXcY63mXlqCi2+whaEoG52S4tBc/ojeozzchhiSyTybraXEHbbwVgKFbnvs2jYi7GtUrNLi1Ht0WbyqG5DgRHee5NQSHPSrh3JAq0M4nhqZESHcmo0e5F65sxVueLShDYLqmB6G9uc1aLzha7jOvtryKK+5e3Wo4IeCkOLgMkFLfNvulCt1U7d8JmX/ABm8ftFGM56I+zbEB3wZLUhexKQRz86Ku+TYTdLYq1Tckhoj3tmRCaUl7Yu8RLKw2dvKSo7VGynDo0GXAayOJyVpbaiySXfCY/uUh3m2BKqtNswjCr5bLdDvKYdxmwGYbcJTqd5iUqPJvFATsXAdwFVmkXBrrIlSr3lRtrsCMq1zSxIQ3s1OHGGXONC9isJ3G1Q7nY7fCsURN3aW1IiAQnHFjiktsNgladgN9k85NIy/F3ZNrhN5BDMi5NpchNhzdb6FbgKR6wdqB37zOMiGKYre78kAuxY+7KVc4LyzwoBFar6gz7/aW8Xn21tEyAYb8yUwohsSXGCVtcB322KtqzH+fwv7FXnsiK07J/VOAdfXz3eKgee9BfqLHal1NJ/dP1pcPedNeTrT1jE95VqB8tumH0UT89ytU+jVHr2x9mcp8nkdFep7p7vTVq+OWhfUkD/0oejvNXbizIueJY0+QYiHXr5ch/lLekqA/wB9XW3vuaWS8unJPdl/yVT3EfS2225/2JrMdu78M+xd57KitO/NOn/X187AKg+etBPqLHal1N+abrSf7zprydausYnvKtQPlt0v+hifnuVqn0ao9fWPsy6f/g6K9T3T3eKtXxy0K6kgfi5Q7zPru9ebnmdxiq5Ry4T4+MWwJ9LUdQcfI9ilbCtarQ1YNLMZsjQ8GDKjMf1KWF7msx/nsL+xV57IitO/NOn/AF9fOwCoPnrQT6ix2pdTfmm60n+86a8nWrrGJ7yrUD5bdL/oYn57lap9GqPX1j7Mun/4OivU9093irV8ctCupIH4ud5ml+RjOK32+KVsqLEcLXtdUOFsfeo1glhXPzvELE54beOQDdJx9c6V447+0FaE/dX6RfxIgdao/KXWY/z+F/Yu89kRWnfmnT/r6+dgFQfPWgn1FjtS6m/NN1pP95015OtXWMT3lWoHy26X/QxPz3K1T6NUevrH2ZdP/wAHRXqe6e7xVq+OWhXUkD8XKHo+HVmNNujOM2gQZDlpXcO7bq+2gqbRGhILvAsj0qrQuC/LgZJm05G0u+XFwp9jTRJ2Hs4jX6RfPhMDrRH5S6zHbu3DDuPiXeeyorTwEWrAOvr52AVB89aC/UWe0rNTfmnO3+KXH3nTfRrT1jD95VqAP326YfRQ/wA9ytUwdtUevbH2ZdPg8hot1Pc+wCrV8ctC+pIP4roez294hCGxwtoCRuTsBsNzTzDMhIQ+yhxIO4C0hQ36PTSmGV8PGy2rhSUDdAOySNiB7DSIkVsJCIrKQkkpAbSNirpI/rQixAW1CKzu2NkHk07pG++w9VGJFPBvFZ3Qd0+LT4J335q7jieM/srPhndfi0+EQd9z6+elR2FuIdWw2pxG3CsoBUnb1GlxYrvHykZpXGUlfE2k8RSNgT69q5Bk8n4lvxYKUeCPBBGxA9VCJFCmliM0FNgBBCBugDoCfVt8KVkgGuI1xGuI1ua4jW5rc1ua3NcRriNcRrc7iga//8QAPhEAAgEDAQUCCAoLAAAAAAAAAQIDBAURAAYSEyExEBQHIjI1QlFhtBUlMzRTcYGxs8MjRHJ0dYKDkpOhov/aAAgBAgEBPwC222pu1X3SmK8Qq75dt1QqDeJJOotmLpJU1lMwijankWN2klVU338lQx5EnVPs1dKlZWVEVkleII8gV3kj5sqA9SNGw3FaFbi6BadlDBiwGSXKAD25Gp9nbjT1Ro3MRmWB53VZAxRYxvMGx0On2crUpqSqE9MyVLqkQWdSxZiBjHszz1dLXUWmfu9TJE0nPIjkD7pBxg46Hssl2NmuArREZMRyRkK5Q4kUrkMOhGqTaFrncZoZLWk3fKqCSOLiMu7LEN1SWHXPpaprnWpNcxV0ULVdvnqaxZHkKCOSQ7reKPL59Bo3GvqbTa7LPRIYZ1ApmLgMJTKf0nrHXd1NcquojqKyCgHCoKaWhlZ5S8mJwVyWIBIXHIaeoraW3WGeWjxTwVEssUmflDvKxHsxu6v13S81hq1gkjZgd7fmMvPJPIkDAGenYep1sds1cbnV0tbBI0NOJWjeWNgsijd5lc/XjUGzEddQVNxnuNUZpl3ZPJO+FL9f8Q0uxNLx6L4yqsxK/DPi5ThtKRjl601JsylLU1VDFcqoQ1FNUTzDxcO0UKyjIx631VWBZrVZYpa+oeFuJuRndwmKcTcuXrOtrLBTWGaiip5XkE0TuxfHVZGTlj6uwDLYGrT8R02y1pHKWqnXiD2BTI//AEwH2atfmA/zfn6X5en+qf759VvnU/w+t90i1N5ssP8AW9xGvCT87tP7vN+O/Zs7SRVt6oYZ2URCUPJk+gnjEfbjU9f3rwhWmmDgpSMIjjpvlSz/AOzq1kCwHmPS/P0pHHg5jpP98+qwj4V6j5hW+6RamI+DLFz+m9xGvCSQaq04P6vN+O/YQQxwcaDyK2+HIbrnPPQmmAwJXx+0dd4n+mf+4640+cmZ89PKOuNNyHFfl08Y6Z5JMb7lsdMknWNf/8QANxEAAgEDAgEGCwkBAAAAAAAAAQIDABESBCExBRAUImGyEzM0QlJxcrGz0dIVIyQyUWJzgYLC/9oACAEDAQE/AJ5kgTwj3tcDbto66FUjfrHMZAAXNhT66FCoJJuA1wLgA8Ca6XEZTCCcxxH9XpNbE6CQZYlgoJFrk0NbGXdMXBQXPVNgKgnWdckDW7Rbm1UHSIsMrbg8L8DUmjEMIYTFcEYE2B2anhitD4ORgkqohAF7ge6hDEs02oSQ5qeuANrW4UsMaFI2l3kdZAAth1aCRyTalBJ12QBh+m1aXTHTR4ZAgcLLbn5R1sMMckTAF8QQCNjvT6wxypCsSYruvZw+dfaMgWT7pNyL9twPnXTS0aSmFMkdVXsBYik1RE85EShha5F7m7Y1oNU+qErOoGJA27QDzE2FT/iX10/FUU29wqbypfV9FeY/tJ7kpfJz/LH3zSeP1P8Aj4hrkf8AJqPaXuDm1jtHp5GUHK1h6zSw4ck6h7byXI9QNhUoPShseH0UQcX2PFP+KS/RzsfGx980vj9TsfN+Ia5H2XUD9y90c9gRjYWrBPRFeDT0FrBOGIrBPRFAKt7ACr1//9k='    

    
            var doc = new jsPDF()

        doc.setFontSize(16);
        doc.setDrawColor(0);
        doc.setFillColor(238, 238, 238);     
            
        doc.text('Booksite Afrika Courier Estimate', 40, 20)
        doc.addImage(imageData, 'JPEG', 10, 10, 25, 20)
    
        doc.setFontSize(10)
        doc.text(dateTime, 40, 28)
        doc.line(10, 35, 200, 35)
        
          doc.setFontSize(14);
        doc.text('Courier Company: ', 15, 50)
        doc.text('Service Level: ', 15, 60)
    
        if(company == 'rtt'){
            doc.text('RTT Logistics', 75, 50)
        }
    else if(company == "courierit"){
         doc.text('CourierIt', 75, 50)
    }
        doc.text(serviceLevel, 75, 60)
        
        doc.setFontSize(12);
    
            doc.setFontSize(12);
        doc.text('Address: ', 15, 75)
        doc.text('City: ', 15, 80)
         doc.text('Country: ', 15, 85)
         doc.text('Postcode: ', 15, 90)
    
            doc.text(address, 75, 75)
        doc.text(city, 75, 80)
         doc.text(country, 75, 85)
         doc.text(postcode, 75, 90)
    
        var startx = 105;
    
        Object.keys(data).forEach(function(key) {
            console.log(key, data[key]);
            var string2 = JSON.stringify(data[key])
            var string = key + ": "   
            doc.text(string, 15, startx);
            doc.text(string2, 75, startx);
            startx = startx + 5;
            })

        
    
    var totalcost = Math.round(data['Total_Cost'] * 100) / 100
    
    doc.setFontSize(16);
    
    doc.text('Total Estimate: ', 15, startx+5);
    doc.text('R'+totalcost, 75, startx+5);
    doc.line(10, startx+20, 200, startx+20)
    
 


    var fileName = 'estimate_' + dateTime + '.pdf';
        doc.save(fileName)
        
    
}