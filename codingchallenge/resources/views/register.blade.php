<!-- Stored in resources/views/child.blade.php -->

@extends('layouts.app')

@section('title', 'Coding Challenge')

@section('nav')
    @parent


@endsection

@section('content')
    <br>
    <br>


        <div class="card">
            <div class="card-body">


                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div>
                </div>
                    <div class="row">
        <div class="col-lg-12 text-center">
            <h1 class="mt-5">Register</h1>
            <form  action="{{ route('submitreg') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="username">Name:</label>
                    <input  name="username" class="form-control" id="username">
                </div>
                <div class="form-group">
                    <label for="massInput">Password:</label>
                    <input name="password" type="password" class="form-control" id="password">
                </div>
        </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    <p></p>

    </div>
    </div>
@endsection