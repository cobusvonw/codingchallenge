<!-- Stored in resources/views/layouts/app.blade.php -->

<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>TaxTim Movie Cinema</title>
  <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css" >
<style>
html {
  position: relative;
  min-height: 100%;
}
body {
  margin-bottom: 60px; /* Margin bottom by footer height */
}
.footer {
  position: absolute;
  bottom: 0;
  width: 100%;
  height: 60px; /* Set the fixed height of the footer here */
  line-height: 60px; /* Vertically center the text there */
  background-color: #f5f5f5;
}
</style>
</head>
<body>
@section('nav')
  <!-- Navigation -->
  <nav class="navbar navbar-expand navbar-dark bg-dark static-top">
    <div class="container">
      <a class="navbar-brand" href=""><img src="{{URL::asset('/img/taxtim.jpg')}}" alt="logo" height="92" width="114"><strong><br>TaxTim Movie Cinema</strong></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="{{ route('register')}}">Register</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="{{ route('login')}}">Log In</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
@show

<div class="container">
  @yield('content')
</div>

@section('footer')
 <footer class="footer mt-auto">
        <div class="container-fluid">

                <span class='text-muted'>
                  TaxTim Movie Cinema
                </span>
           

          <div class="copyright float-right">
            &copy;Cobus von Wielligh
            <script>
              document.write(new Date().getFullYear())
            </script>

          </div>
          <!-- your footer here -->
        </div>
      </footer>
@show

<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
</body>
</html>