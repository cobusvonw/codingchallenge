<!-- Stored in resources/views/child.blade.php -->

@extends('layouts.app')

@section('title', 'Coding Challenge')

@section('nav')
    @parent


@endsection

@section('content')
    <br>
    <br>
    <div class="row">
        <div class="col-lg-12 text-center">
            @if(isset($user->username))

                <h3 class="mt-2">Welcome back, {{$user->username ?? ''}}</h3>
            @else
                <h3 class="mt-2">Please login to book a movie.</h3>
            @endisset

            <h1 class="mt-2">Now Showing at Cinema 1:</h1>
        <br>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 text-center">
                <div class="card">
                    <div class="card-body">
                        <a href="#"><img src="{{URL::asset('/img/django.jpg')}}" class="img-fluid" alt="Django Unchained"></a>

                     </div>
             </div>
        </div>
        <div class="col-lg-6 text-center">
            <div class="card">
                <div class="card-body">
                    <a href="#"><img src="{{URL::asset('/img/pulp.png')}}" class="img-fluid" alt="Pulp Fiction"></a>

                </div>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-lg-12 text-center">
            <h1 class="mt-2">Now Showing at Cinema 2:</h1>
        </div>

        <div class="col-lg-6 text-center">

            <div class="card">
                <div class="card-body">
                    <a href="#"><img src="{{URL::asset('/img/lion.jpg')}}" class="img-fluid" alt="Lion King"></a>

                </div>
            </div>
        </div>
        <div class="col-lg-6 text-center">
            <div class="card">
                <div class="card-body">
                    <a href="#"><img src="{{URL::asset('/img/toy.jpg')}}" class="img-fluid" alt="Toy Story"></a>

                </div>
            </div>
        </div>
    </div>


@endsection