<?php
namespace App\Http\Controllers;

use View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Upload;



class MainController extends Controller{



    public function processUserRegister(Request $request){
        //TODO: Password encryption (should have used laravel's Authenticate, wasted time, bootleg approach)
        $errors = null;
        $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        DB::table('users')->insert(
            array(
                'username'     =>   $request->username,
                'password'   =>     $request->password
            )
        );


        return \View::make("login");

    }

    public function processUserLogin(Request $request){
        $errors = null;
        $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);


        $currentUser = DB::table('users')->where('username', $request->username)->first();

        if($currentUser != null && ($currentUser->password == $request->password)) {

            return \View::make("welcome")->with('user', $currentUser);

        } else {

           //TODO: Handle Incorrect Login

        }



    }




}