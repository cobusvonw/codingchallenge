<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/login', function () {
    return view('login');
},'NavigationController@map')->name('login');


Route::get('/register', function () {
    return view('register');
},'NavigationController@map')->name('register');

Route::post('submitreg', 'MainController@processUserRegister')->name('submitreg');
Route::post('submitlogin', 'MainController@processUserLogin')->name('submitlogin');




